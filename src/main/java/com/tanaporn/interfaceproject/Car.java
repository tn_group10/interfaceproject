/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tanaporn.interfaceproject;

/**
 *
 * @author HP
 */
public class Car extends Vahicle implements Runable {

    public Car() {
        super("Car");
    }

    @Override
    public void startEngine() {
        System.out.println("Car: Start Engine.");
    }

    @Override
    public void stopEngine() {
        System.out.println("Car: Stop Engine.");
    }

    @Override
    public void run() {
        System.out.println("Car: Run.");
    }
}

