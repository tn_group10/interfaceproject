/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tanaporn.interfaceproject;

/**
 *
 * @author HP
 */
public abstract class Vahicle {

    private String name;

    public Vahicle(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Vahicle{" + "name=" + name + '}';
    }

    public abstract void startEngine();

    public abstract void stopEngine();

}

