/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tanaporn.interfaceproject;

/**
 *
 * @author HP
 */
public class TestProgram {

    public static void main(String[] args) {
        Crocodile crocodile = new Crocodile("Susi");
        Snake snake = new Snake("Megan");
        Human human = new Human("May");
        Dog dog = new Dog("Bo");
        Cat cat = new Cat("Pinky");
        Fish fish = new Fish("Dorry");
        Crab crab = new Crab("Capu");
        Bat bat = new Bat("Covid");
        Bird bird = new Bird("Petie");
        Car car = new Car();
        Plane plane = new Plane();

        Crawlable[] crawlable = {crocodile, snake};
        for (Crawlable craw : crawlable) {
            if (craw instanceof Reptile) {
                Reptile rep = (Reptile) craw;
                rep.eat();
                rep.walk();
                rep.crawl();
                rep.speak();
                rep.sleep();
            }
        }
        
        System.out.println("");
        
        Runable[] runable = {human, dog, cat, car, plane};
        for (Runable run : runable) {
            if (run instanceof LandAnimal) {
                LandAnimal land = (LandAnimal) run;
                land.eat();
                land.walk();
                land.run();
                land.speak();
                land.sleep();
            } else {
                if (run instanceof Car) {
                    Car c = (Car) run;
                    c.startEngine();
                    c.run();
                    c.stopEngine();
                } else if (run instanceof Plane) {
                    Plane p = (Plane) run;
                    p.startEngine();
                    p.run();
                    p.stopEngine();
                }
            }
        }
        
        System.out.println("");
        
        Swimmable[] swimmable = {fish, crab};
        for (Swimmable swim : swimmable) {
            if (swim instanceof AquaticAnimal) {
                AquaticAnimal aqua = (AquaticAnimal) swim;
                aqua.eat();
                aqua.walk();
                aqua.swim();
                aqua.speak();
                aqua.sleep();
            }
        }
        
        System.out.println("");
        
        Flyable[] flyable = {bat, bird, plane};
        for (Flyable f : flyable) {
            if (f instanceof Poultry) {
                Poultry poul = (Poultry) f;
                poul.eat();
                poul.walk();
                poul.fly();
                poul.speak();
                poul.sleep();
            } else if (f instanceof Plane) {
                Plane p = (Plane) f;
                p.startEngine();
                p.run();
                p.stopEngine();
            }
        }
    }

}

